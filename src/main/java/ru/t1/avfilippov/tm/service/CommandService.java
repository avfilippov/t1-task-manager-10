package ru.t1.avfilippov.tm.service;

import ru.t1.avfilippov.tm.api.ICommandRepository;
import ru.t1.avfilippov.tm.api.ICommandService;
import ru.t1.avfilippov.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }
}
