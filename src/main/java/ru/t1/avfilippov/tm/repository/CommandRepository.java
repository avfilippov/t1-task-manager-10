package ru.t1.avfilippov.tm.repository;

import ru.t1.avfilippov.tm.api.ICommandRepository;
import ru.t1.avfilippov.tm.constant.ArgumentConst;
import ru.t1.avfilippov.tm.constant.CommandConst;
import ru.t1.avfilippov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            CommandConst.INFO, ArgumentConst.INFO,
            "show system info"
    );

    private static final Command ABOUT = new Command(
            CommandConst.ABOUT, ArgumentConst.ABOUT,
            "show developer's info"
    );

    private static final Command VERSION = new Command(
            CommandConst.VERSION, ArgumentConst.VERSION,
            "show application version"
    );

    private static final Command HELP = new Command(
            CommandConst.HELP, ArgumentConst.HELP,
            "show command list"
    );

    private static final Command EXIT = new Command(
            CommandConst.EXIT, null,
            "close application"
    );

    private static final Command PROJECT_CREATE = new Command(
            CommandConst.PROJECT_CREATE, null,
            "create new project"
    );

    private static final Command PROJECT_LIST = new Command(
            CommandConst.PROJECT_LIST, null,
            "display all projects"
    );

    private static final Command PROJECT_CLEAR = new Command(
            CommandConst.PROJECT_CLEAR, null,
            "remove all projects"
    );

    private static final Command TASK_CREATE = new Command(
            CommandConst.TASK_CREATE, null,
            "create new task"
    );

    private static final Command TASK_LIST = new Command(
            CommandConst.TASK_LIST, null,
            "display all tasks"
    );

    private static final Command TASK_CLEAR = new Command(
            CommandConst.TASK_CLEAR, null,
            "remove all tasks"
    );

    public static final Command[] COMMANDS = new Command[]{
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            ABOUT, INFO, VERSION, HELP, EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
