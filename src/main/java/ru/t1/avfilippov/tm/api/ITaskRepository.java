package ru.t1.avfilippov.tm.api;

import ru.t1.avfilippov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    List<Task> findAll();

    void clear();

}
