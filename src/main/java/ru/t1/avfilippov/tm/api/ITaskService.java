package ru.t1.avfilippov.tm.api;

import ru.t1.avfilippov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name, String description);

    Task add(Task task);

    List<Task> findAll();

    void clear();

}
