package ru.t1.avfilippov.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
