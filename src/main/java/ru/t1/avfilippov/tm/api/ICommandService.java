package ru.t1.avfilippov.tm.api;

import ru.t1.avfilippov.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
