package ru.t1.avfilippov.tm.api;

public interface ICommandController {

    void showSystemInfo();

    void showAbout();

    void showErrorArgument();

    void showErrorCommand();

    void showVersion();

    void showExit();

    void showHelp();

}
