package ru.t1.avfilippov.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}
